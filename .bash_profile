# [ Bash Configs ]
. ~/.bashrc

# [ Profile ]
source ~/.profile

# [ FZF ]
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# [ Direnv ]
type direnv 1>/dev/null 2>&1 && eval "$(direnv hook bash)"

# [ Starship ]
which starship > /dev/null 2>&1 && eval "$(starship init bash)"

# [ WM Launch ]
window_manager=sway
which river > /dev/null 2>&1 && window_manager=river
if [ "$(tty)" = "/dev/tty1" ]; then
	XDG_CURRENT_DESKTOP=GNOME
	exec $window_manager
	exit 0
fi
