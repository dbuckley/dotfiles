set nocompatible
filetype off


" --------------------------------------------- "
" ------------------ Plugins ------------------ "
" --------------------------------------------- "

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

set rtp+=~/.vim/bundle/Vundle.vim

call plug#begin('~/.vim/bundle')
" Bundles
Plug 'junegunn/vim-plug'               " Plugin manager
Plug 'ap/vim-buftabline'               " Show buffers as tabs
Plug 'autozimu/LanguageClient-neovim', { 'branch': 'next', 'do': 'bash install.sh', } " LSP client
Plug 'christoomey/vim-tmux-navigator'  " Allow Ctrl-{j,k,l,h} to navigate vim/tmux
Plug 'dkarter/bullets.vim'             " Auto bullet/number markdown lists
Plug 'editorconfig/editorconfig-vim'   " Make vim understand editorconfig files
Plug 'gerw/vim-HiLinkTrace'            " Trace highlighting in vim
Plug 'itchyny/lightline.vim'           " Better status line
Plug 'junegunn/fzf'                    " Use fzf for finding files
Plug 'junegunn/fzf.vim'                " Extend fzf searching
Plug 'michal-h21/vim-zettel'           " Extend vimwiki for zettel notes
Plug 'preservim/nerdtree'              " File tree view
Plug 'vimwiki/vimwiki'                 " Wiki functionality with vim
Plug 'tpope/vim-fugitive'              " Git functions
Plug 'tpope/vim-repeat'                " Let's plugins utilize '.'
Plug 'tpope/vim-surround'              " Functions to surround with braces/brackets etc...
Plug 'lifepillar/vim-mucomplete'       " Simple completion
Plug 'shougo/echodoc.vim'              " Display function definitins
" Syntax highlighting
Plug 'LnL7/vim-nix'
Plug 'cespare/vim-toml'
Plug 'leafgarland/typescript-vim'
Plug 'pangloss/vim-javascript'
Plug 'plasticboy/vim-markdown'
Plug 'ziglang/zig.vim'
Plug 'ziglang/zig.vim'
" /Bundles
call plug#end()

set clipboard+=unnamedplus


" --------------------------------------------- "
" ------------------ Settings ----------------- "
" --------------------------------------------- "

set noerrorbells   " No beeps
set noswapfile     " Don't use swapfile
set nobackup       " Don't create annoying backup files
set nowritebackup  " Don't create annoying backup files
set autowrite      " Automatically save before :next, :make etc.
set laststatus=2   " Show status line
set hidden         " Hide buffers
set ignorecase     " Search case insensitive...
set smartcase      " ... but not when search pattern contains upper case characters
set t_Co=256       "support 256 color
set magic          " unbreak vim's regex implementation

set backspace=indent,eol,start      " Makes backspace key more powerful.
set listchars=tab:▸\ ,eol:¬,space:. " Make it easy to see whitespace.

" Navigation and position helpers
set ruler         " Show line,col in bottom bar
set cc=80         " Set a colored column
set number        " Show line numbers
set scrolloff=3   " Lines to keep above/bellow when scrolling
set sidescroll=3  " Columns to keep left/right when scrolling
set showcmd       " Show me what I'm typing
set noshowmode    " We show mode in the lightline plugin

" split intuitively
set splitright 
set splitbelow

" Default file format preferences
set textwidth=79   " Keep proper text width
set encoding=utf-8
set tabstop=8      " Tab width
set shiftwidth=8   " How much to shift when indenting
set autoindent
set nowrap         " Don't wrap long lines
set nofoldenable   " Don't fold things

" Search as you type, highlight results
set incsearch
set showmatch
set hlsearch

" Help vim preform better
set lazyredraw           " Wait to redraw
set nocursorcolumn       " Don't highlight cursor column
set nocursorline         " Don't highlight cursor line
syntax sync minlines=256 " Sync the syntax 256 or more lines
set synmaxcol=300        " Sync out to 300 chars or more

set mouse=a

" Completion helpers (mucomplete and echodoc)
set completeopt-=preview
set completeopt+=menuone,noselect
set cmdheight=2

" --------------------------------------------- "
" ------------------ Auto Commands ------------ "
" --------------------------------------------- "

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
" Also don't do it when the mark is in the first line, that is the default
" position when opening a file.
autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \	exe "normal! g`\"" |
  \ endif

" This trigger takes advantage of the fact that the quickfix window can be
" easily distinguished by its file-type, qf. The wincmd J command is
" equivalent to the Ctrl+W, Shift+J shortcut telling Vim to move a window to
" the very bottom (see :help :wincmd and :help ^WJ).
autocmd FileType qf wincmd J

" Save file when focus is lost.
autocmd FocusLost * :wa


" --------------------------------------------- "
" ------------------ Keybindings -------------- "
" --------------------------------------------- "

"  Set this first so we can use it
let mapleader = "\<space>"

" Gotta save fast
nmap <leader>w :w!<cr>

" Remove the search highlighting
nnoremap \\ :nohlsearch<CR>

" Move up and down on splitted lines (on small width screens)
map <Up> gk
map <Down> gj
map k gk
map j gj

" Just go out in insert mode
imap jk <ESC>l

" Set local to spelling for "autocorrect"
nnoremap <leader>ac :setlocal spell! spell?<CR>

" Center the screen when searching through items
nnoremap n nzzzv
nnoremap N Nzzzv

" Have Y act like D and C
nnoremap Y y$

" Do not show stupid q: window
map q: :q

" Allow saving of files as sudo
cmap w!! w !sudo tee > /dev/null %

" Some useful quickfix shortcuts
":cc      see the current error
":cn      next error
":cp      previous error
":clist   list all errors
map <leader>cn :cn<CR>
map <leader>cp :cp<CR>

" FZF open file
map <leader>o :FZF<CR>

" Show current directory
nnoremap <leader>. :lcd %:p:h<CR>

" Trim trailing whitespace
nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

" Toggle list view
nmap <leader>l :set list!<CR>

" Searching
nnoremap <leader>f :FZF<CR>

" Grepping
if executable('rg')
  nnoremap <leader>g :Rg<CR>
elseif executable('ag')
  nnoremap <leader>g :Ag<CR>
else
  nnoremap <leader>g :GFiles<CR>
endif

" auto-closing done right
inoremap (<CR> (<CR>)<C-c>O
inoremap (; (<CR>);<C-c>O
inoremap (, (<CR>),<C-c>O
inoremap {<CR> {<CR>}<C-c>O
inoremap {; {<CR>};<C-c>O
inoremap {, {<CR>},<C-c>O
inoremap [<CR> [<CR>]<C-c>O
inoremap [; [<CR>];<C-c>O
inoremap [, [<CR>],<C-c>O

" --------------------------------------------- "
" ------------------ Buffers -------------- "
" --------------------------------------------- "

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
    \ | wincmd p | diffthis
endif


" Replace the current buffer with the given new file. That means a new file
" will be open in a buffer while the old one will be deleted
com! -nargs=1 -complete=file Breplace edit <args>| bdelete #

" Buffer prev/next
nnoremap <C-x> :bnext<CR>
nnoremap <C-z> :bprev<CR>

" Show directory when opening or switch to a buffer
autocmd BufEnter * silent! lcd %:p:h


" --------------------------------------------- "
" ------------------ FileType ----------------- "
" --------------------------------------------- "

filetype plugin indent on
syntax on

" Reminder
" noet -> noexpandtab = Don't expand a tab into spaces
" ts -> tabstop = How many spaces a tab is worth (tab width)
" tw -> textwidth = Max width of text. Text going past will be formatted.

" C/C++
autocmd FileType c setlocal noet ts=8 tw=80
autocmd FileType h setlocal noet ts=8 tw=80
autocmd FileType cpp setlocal noet ts=4 tw=80

" Go
autocmd FileType go setlocal noet ts=4

" Bourne Shell
autocmd FileType sh setlocal noet ts=4 sw=4

" HTML/Javascript/etc
autocmd BufRead,BufNewFile *.js setlocal et ts=2 sw=2
autocmd FileType html setlocal et ts=2 sw=2
autocmd FileType typescript setlocal et ts=2 sw=2

" Markup/Text
autocmd FileType yaml setlocal et ts=2 sw=2
autocmd FileType markdown setlocal tw=80 et ts=2 sw=2
autocmd FileType markdown set spell spelllang=en
autocmd FileType text setlocal tw=80
autocmd BufNewFile,BufRead *.scd set ts=4 sw=4 noet

" Build tools
autocmd FileType meson setlocal noet ts=2 sw=2

" Python
autocmd FileType python setlocal et ts=4 sw=4

" Zig
autocmd FileType zig set cc=100 et ts=4 sw=4 tw=0


" --------------------------------------------- "
" ------------ Syntax Highlighting ------------ "
" --------------------------------------------- "

colorscheme pablo
set background=light
highlight Search ctermbg=12
highlight NonText ctermfg=darkgrey
highlight SpecialKey ctermfg=darkblue
highlight Comment cterm=bold ctermfg=grey
highlight Keyword ctermfg=blue
highlight Statement ctermfg=yellow
highlight Operator ctermfg=magenta
highlight ColorColumn ctermbg=darkgrey guibg=lightgrey
highlight Todo ctermbg=NONE ctermfg=red cterm=bold
highlight PreProc ctermfg=grey
highlight zigBuiltinFn ctermfg=darkgrey
highlight String ctermfg=cyan cterm=italic
highlight Type ctermfg=magenta
highlight lineNr ctermfg=grey cterm=italic
highlight cIncluded ctermfg=darkcyan cterm=bold
highlight pythonInclude ctermfg=darkcyan


" --------------------------------------------- "
" ------------------ Plugin Configs ----------- "
" --------------------------------------------- "

""""""""""""""""""""
" LanguageClient

let g:LanguageClient_serverCommands = {
    \ 'bash': ['bash-language-server'],
    \ 'c': ['ccls'],
    \ 'go': ['gopls'],
    \ 'nix': ['rnix-lsp'],
    \ 'python': ['pyls'],
    \ 'rust': ['rustup'],
    \ 'typescript': ['javascript-typescript-stdio'],
    \ 'zig': ['zls'],
    \ }

let g:LanguageClient_autoStart = 1
let g:LanguageClient_loadSettings = 1

" Fallback to builtin searchdecl if languageClient fails
function GotoDefinitionHandler(output) abort
    if has_key(a:output, 'error')
      call searchdecl(expand('<cword>'))
    endif
endfunction

function! GotoDefinition() abort
    call LanguageClient#textDocument_definition({'handle': v:true}, 'GotoDefinitionHandler')
endfunction

nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call GotoDefinition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" Format on save
augroup fmt
  autocmd!
  autocmd BufWritePre * undojoin | call LanguageClient_textDocument_formatting()
augroup END
map <leader>fmt :autocmd! fmt<CR>

""""""""""""""""""""
" Lightline

let g:lightline = {
      \ 'colorscheme': '16color',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head',
      \ },
      \ }

""""""""""""""""""""
" mucomplete

let g:mucomplete#enable_auto_at_startup = 1
let g:mucomplete#completion_delay = 0
let g:echodoc#enable_at_startup = 1

""""""""""""""""""""
" vimwiki

let g:vimwiki_global_ext = 0
let g:vimwiki_list = [
        \ {
            \ 'path':'~/.notes/zettel/',
            \ 'ext':'.md',
            \ 'syntax':'markdown',
        \ },
    \ ]

let g:zettel_format = "%y%m%d-%H%M"

""""""""""""""""""""
" bullets

let g:bullets_enable_file_types = [
	\ 'gitcommit',
	\ 'markdown',
	\ 'text',
	\]


""""""""""""""""""""
" vim-markdown

let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_folding_disabled=1
let g:vim_markdown_frontmatter=1
let g:vim_markdown_toml_frontmatter=1
