pkgs : {
  allowUnfree = true;
  packageOverrides = pkgs : with pkgs; rec {
    default-desktop-apps = pkgs.buildEnv {
      name = "desktop-apps";
      paths = [
        bitwarden
        pavucontrol
        qutebrowser
        spotify
      ];
    };

    gaming-apps = pkgs.buildEnv {
      name = "gaming-apps";
      paths = [
        steam
      ];
    };

    defaults = pkgs.buildEnv {
      name = "all";
      paths = [
        fzf
        go
        tmux
      ];
    };

    host-paris = pkgs.buildEnv {
      name = "host-paris";
      paths = [
        gaming-apps
        default-desktop-apps
        defaults
      ];
    };

    host-bordeaux = pkgs.buildEnv {
      name = "host-bordeaux";
      paths = [
        default-desktop-apps
        defaults
      ];
    };

    #####
    # Dev tool grouping

    c-build = pkgs.buildEnv {
      name = "c-build";
      paths = [
        cmake
        gcc
        gdb
        meson
      ];
    };
  };
}
