import os

config.load_autoconfig()
c.window.hide_decoration = True

if (os.getenv("USE_QWERTY") == None):
    config.bind("h", "scroll left")

    config.bind("n", "scroll down")
    config.bind("N", "tab-prev")
    config.bind("k", "search-next")
    config.bind("K", "search-prev")

    config.bind("e", "scroll up")
    config.bind("E", "tab-next")

    config.bind("i", "scroll right")
    config.bind("I", "forward")
    config.bind("l", "mode-enter insert")

    c.hints.chars = "arstdhneio"
