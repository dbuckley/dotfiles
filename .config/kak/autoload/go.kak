hook global WinSetOption filetype=go %{
    make-width 120
    set-option window formatcmd 'gofmt | goimports'

    lsp-enable-window
    lsp-auto-hover-enable
}
