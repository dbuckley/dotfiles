hook global BufOpenFile .* %{ evaluate-commands %sh{
    if [ "$(dirname ${kak_buffile})" == "$ZETTEL_DIR" ]; then
        printf "require-module zettel\n"
    fi
} }

provide-module zettel %{

define-command open-zettel -docstring %{
    Search for zettel with the given tag(s) and open.
} -params .. %{ prompt "tags: " %{eval %sh{
    set -x
    src_cmd="zectl find "$@" $kak_text"
    filter_cmd="sed -E 's%([^:]+).*%$ZETTEL_DIR/\1%'"

    printf "%s\n" "fuzzy -input-cmd %{$src_cmd} -kak-cmd %{edit} -filter %{$filter_cmd}"
}}}

define-command link-zettel -docstring %{
    Search for zettel with the given tag(s) and create Markdown link.
} -params .. %{ prompt "tags: " %{eval %sh{
    src_cmd="(zectl find "$@" $kak_text)"
    filter_cmd="sed -E 's/([^:]+).*/execute-keys i[<esc>a](\1)<esc>/'"

    printf "%s\n" "fuzzy -input-cmd %{$src_cmd} -kak-cmd %{evaluate-commands} -filter %{$filter_cmd}"
}}}

}
