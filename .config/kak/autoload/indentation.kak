define-command expandtab %{
    remove-hooks global expandtab
    hook -group expandtab global InsertChar '\t' %{ execute-keys -draft h@ }
    hook -group expandtab global InsertDelete ' ' %{ try %{
        execute-keys -draft -itersel "<a-h><a-k>^\h+.\z<ret>I<space><esc><lt>"
    }}
    set-option global aligntab false
    remove-hooks global noexpandtab
    remove-hooks global smarttab
}
