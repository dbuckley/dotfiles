hook global WinSetOption filetype=nix %{
    set-option window formatcmd 'nixpkgs-fmt'
    expandtab
    lsp-enable-window
}
