hook global WinSetOption filetype=zig %{
    make-width 120
    set-option window formatcmd 'zig fmt --stdin'

    expandtab
    lsp-enable-window
}
