declare-option -docstring 'Terminal to pop open.
Supported terminals:
    foot (default)
    alacritty' \
    str fuzzy_terminal 'foot'

define-command fuzzy-file %{ try %sh{
    if git rev-parse --is-inside-work-tree > /dev/null 2>&1; then
        src_cmd="git ls-files --recurse-submodules"
    else
        src_cmd="rg --files"
    fi

    flags="--preview 'bat --style=numbers --color=always --line-range :500 {}'"
    printf "%s\n" "fuzzy -input-cmd %{$src_cmd} -extra-flags %{$flags}"
}}

define-command fuzzy-cd %{ try %sh{
    input_cmd="find . \( -path '*/.svn*' -o -path '*/.git*' \) -prune -o -type d -print"
    printf "%s\n" "fuzzy -input-cmd %{$input_cmd} -kak-cmd %{change-directory}"
}}

define-command -hidden fuzzy -params .. %{ evaluate-commands %sh{
    while [ $# -gt 0 ]; do
        case $1 in
            (-input-cmd)    shift; input_cmd="$1";;
            (-extra-flags)  shift; flags="$1";;
            (-kak-cmd)      shift; kak_cmd="$1";;
            (-filter)       shift; filter="$1";;
        esac
        shift
    done

    input_cmd="${input_cmd:-find . -f}"
    kak_cmd="${kak_cmd:-edit -existing}"
    filter="${filter:-cat}"

    terminal="${kak_opt_fuzzy_terminal}"

    case "$terminal" in
        (alacritty)  spawn="alacritty --option \"window.dimensions={lines: 10, columns: 10}\" --class float -e sh -c";;
        (foot|*)     spawn="foot -W 100x35 --app-id=float sh -c";;
    esac

    pipe="$XDG_RUNTIME_DIR/kak_fuzzy_pip"
    [ ! -e "$pipe" ] && mkfifo "$pipe"

    setsid $spawn "${input_cmd} | fzf ${flags} | ${filter} > ${pipe}" &

    echo "eval -client ${kak_client} ${kak_cmd}  %{$(cat $pipe)}" | kak -p ${kak_session}
}}
