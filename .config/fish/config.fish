# [ Profile ]
if [ -z "$PROFILE_SET" ]
	cat ~/.profile | babelfish | source
end

# Launch sway
set -x window_manager sway
which river > /dev/null 2>&1 && set -x window_manager river
if [ (tty) = "/dev/tty1" ]
	set XDG_CURRENT_DESKTOP GNOME
	exec $window_manager
	exit 0
end
