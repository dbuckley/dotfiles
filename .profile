export PROFILE_SET=1
# ---------------------------------------------------------------------
# [ SYSTEM ]
# ---------------------------------------------------------------------

# [ Editor ]
# Use kak, nvim or vim whichever is available.
export EDITOR=$(which vim)
if type kak 1>/dev/null 2>&1; then
	export EDITOR=$(which kak)
	alias vim=kak
elif type nvim 1>/dev/null 2>&1; then
	export EDITOR=$(which nvim)
	alias vim=nvim
fi

# [ XDG ]
export XDG_DATA_HOME="$HOME"/.local/share
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_CACHE_HOME="$HOME"/.cache

# [ PATH ]
export PATH="$HOME.local/bin/$(uname -m):$HOME/.local/bin${PATH:+:${PATH}}"
export PATH="$HOME/.local/bin/$(uname -m):$HOME/.local/bin:$HOME/bin:$PATH"

# [ NIX_PATH ]
[ -d $HOME/src/custom_nix_pkgs ] && export \
	NIX_PATH=custompkgs=$HOME/src/custom_nix_pkgs/default.nix:$NIX_PATH

# [ Host Spacific ]
[ -e $HOME/."${HOSTNAME}"_profile.local ] && . $HOME/."${HOSTNAME}"_profile.local

# ---------------------------------------------------------------------
# [ DEV ]
# ---------------------------------------------------------------------

export GOPATH="$XDG_CACHE_HOME"/go
export PATH="$PATH:$GOPATH/bin"

# ---------------------------------------------------------------------
# [ APP ENV ]
# ---------------------------------------------------------------------

export LESS='-giR -P"%F"'
export WEECHAT_HOME="$XDG_CONFIG_HOME"/weechat

# ---------------------------------------------------------------------
# [ USER ]
# ---------------------------------------------------------------------

# Set note dir
if [ -d $HOME/.notes ]; then
	export NOTE_DIR=$HOME/.notes
elif [ -d $HOME/Documents/notes ]; then
	export NOTE_DIR=$HOME/Documents/notes
else
	export NOTE_DIR=$HOME
fi
export ZETTEL_DIR=$NOTE_DIR/zettel

# Used to join all arguments with arg 1.
# ie: _join_array '_' 'these are args' -> 'these_are_args'
_join_array() {
	local IFS="$1"
	shift
	echo "$*"
}

# [ Note Helpers ]
# join all args with '_' and open markdown file in $EDITOR
nnote() {
	name=$(join_array _ $@)
	$EDITOR $name.md
}

# join all args with '_', append with YY-MM-DD date and open markdown file
# in $EDITOR
dnote() {
	nnote $(date +%y-%m-%d) $@
}

alias dn="date +%y%m%d"            # A standard date timestamp
alias stamp="date +%Y%m%d%H%M%S"   # A standard date timestamp
alias bullet="bullet -m"           # Always migrate tasks
alias fzf="fzf-tmux"               # FZF with Tmux
alias t="cd $(mktemp -d /tmp/scratch-XXXX)" # Scratch temp directories

# [ Tmux Workspaces ]
alias tmx="tms -n dbuckley"
alias tf="tms -f"
alias tj="tms -j"

fe() {
	local files
	IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
	[[ -n "$files" ]] && $EDITOR "${files[@]}"
}

##########
# Zettelkasten note helpers

# [ Zettelkasten ]
# Get zet
gzet() {
	$EDITOR $(find $ZETTEL_DIR -name '*.md' | \
		fzf --preview='head -$LINES {}' --height 10)
}

# New zet
nzet() {
	if [ -z $1 ]; then
		echo 2>&1 "usage: nzet [NAME...]"
		return 1
	fi

	$EDITOR "$ZETTEL_DIR/$(join_array '_' $(date +%y%m%d%H%M%S) $@).md"
}
