# [ Global Bash ]
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# [ Bash Options ]
set -o vi
export HISTCONTROL=ignoreboth:erasedups
