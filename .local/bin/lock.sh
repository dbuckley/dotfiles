#!/usr/bin/env bash

grim /tmp/screenshot.png
convert /tmp/screenshot.png -blur 0x6 /tmp/screenshot.png
swaylock -i /tmp/screenshot.png
