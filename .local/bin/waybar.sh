#!/usr/bin/env sh

# Terminate already running bar instances
ps x | grep -v grep | grep 'bin/waybar$' | awk '{print $1}' |
	while read pid; do
		kill $pid;
	done

# Wait until the processes have been shut down
while pgrep -x waybar >/dev/null; do sleep 1; done

# Launch main
waybar
