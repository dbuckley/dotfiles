package main

import (
	"fmt"
	"os"
	"os/user"
	"path"
	"strings"

	git "gopkg.in/src-d/go-git.v4"
)

func main() {
	prompt := New()
	prompt.User()
	prompt.WriteRune('@')
	prompt.Hostname()
	prompt.Space()
	prompt.CWD()
	prompt.Space()
	if err := prompt.GitBranch('('); err == nil {
		prompt.Space()
	}
	prompt.WriteRune('$')
	fmt.Printf(prompt.String())
}

type Prompt struct {
	*strings.Builder

	cwd  string
	user *user.User
}

func New() *Prompt {
	return &Prompt{
		Builder: &strings.Builder{},
	}
}

func (p *Prompt) getwd() string {
	if p.cwd == "" {
		p.cwd, _ = os.Getwd()
	}
	return p.cwd
}

func (p *Prompt) getUser() *user.User {
	if p.user == nil {
		p.user, _ = user.Current()
	}
	return p.user
}

func (p *Prompt) WriteString(s string) {
	p.Builder.WriteString(s)
}

func (p *Prompt) WriteRune(b rune) {
	p.Builder.WriteRune(b)
}

func (p *Prompt) Space() {
	p.Builder.WriteRune(' ')
}

func (p *Prompt) String() string {
	return p.Builder.String()
}

func (p *Prompt) User() {
	u := p.getUser()
	p.WriteString(u.Username)
}

func (p *Prompt) Hostname() {
	hn, _ := os.Hostname()
	p.WriteString(hn)
}

func (p *Prompt) GitBranch(wrapping rune) error {
	var err error
	cwd := p.getwd()
	repo, err := git.PlainOpen(cwd)
	if err != nil {
		return err
	}

	h, err := repo.Head()
	if err != nil {
		return err
	}

	var gitStat string
	switch wrapping {
	case '(':
		gitStat = fmt.Sprintf("(%s)", path.Base(string(h.Name())))
	case '{':
		gitStat = fmt.Sprintf("{%s}", path.Base(string(h.Name())))
	default:
		gitStat = fmt.Sprintf("%s", path.Base(string(h.Name())))
	}
	p.WriteString(gitStat)
	return nil
}

func (p *Prompt) CWD() {
	cwd := p.getwd()
	home := os.Getenv("HOME")

	var parts []string
	if strings.HasPrefix(cwd, home) {
		cwd = "~" + cwd[len(home):]
	}

	parts = strings.Split(cwd, "/")
	for i, part := range parts {
		if i == len(parts)-1 {
			p.WriteString(fmt.Sprintf("%s", part))
		} else {
			if len(part) != 0 {
				p.WriteString(fmt.Sprintf("%c/", part[0]))
			} else {
				p.WriteString(fmt.Sprintf("/"))
			}
		}
	}
}
