#!/bin/sh -
#
#       CREATED : September 28, 2019
#          FILE : applications.sh
#   DESCRIPTION : wrapper arround `wofi` with fallback to using terminal window
#   			  and `fzf`.
#                   .desktop files by name
#                   & laptops)
#==============================================================================

#==============================================================================
# Functions
#==============================================================================

##########
# Generate list of executables
executable_list() {
    echo -n "$PATH" | \
		xargs -d: -I{} -r -- \
		find -L {} -maxdepth 1 -mindepth 1 \
		-type f -executable -printf '%P\n' 2>/dev/null | \
		sort -u
}

##########
# List all files with the desktop extension
__find_all_desktop() {
	local _data_dirs=$XDG_DATA_DIRS
	if [ "{$_data_dirs#*/usr/share/applications}" == $_data_dirs ]; then
		_data_dirs=$_data_dirs:/usr/share/applications
	fi

	echo $XDG_DATA_DIRS | tr ':' '\n' | while read dir; do
		[ -d $dir/applications ] && \
			find -L $dir/applications/ -type f -name '*.desktop'
	done
}

##########
# list all apllications on system by Name from the .desktop
desktop_list() {
	local _applications=$(__find_all_desktop | while read desktop; do
			grep 'Name=' $desktop | cut -d '=' -f2
		done
	)
	echo "$_applications" | sort | uniq

	if type flatpak > /dev/null 2>&1 ; then
		flatpak list | grep -vi platform | grep -iv theme | cut -d$'\t' -f1
	fi
}

##########
# Runs an app given a .desktop file
_desktop_run() {
	local _desktop_file=$1; shift

	local _run_app=`echo $_desktop_file |\
	cut -d ':' -f1 | \
	xargs grep 'Exec=' | \
	cut -d '=' -f2- | \
	sed 's/%[uU]//g' | \
	grep -v ' %' | \
	head -n 1`
	$_run_app
}


##########
# Runs an application by first checking flatpaks, then the .desktop files
# and lastly running from a terminal
desktop_run() {
	local _app=$@
	if type flatpak > /dev/null 2>&1 ; then
		if flatpak list | grep -q "$_app" > /dev/null; then
			local _run_app=`flatpak list | grep $_app | cut -d$'\t' -f2`
			echo $_run_app
			flatpak run "$_run_app" &
			return
		fi
	fi
	__find_all_desktop | while read desktop; do
		if (grep -q Name="$_app" $desktop); then
			_desktop_run $desktop &
			return
		fi
	done
	alacritty -e "$_app" &
	return
}

##########
# Opens a alacritty window with the class 'launcher'
fzf_menu() {
	local _input=""
	while read line; do
		_input+=$line
		_input+=":"
	done

	alacritty --class=launcher -e bash -c "
		echo \"$_input\" |
		tr ':' '\n' |
		sort -u |
		fzf --layout=reverse |
		xargs -r -I {} swaymsg -t command exec applications.sh desktop_run {}"
}

##########
# Run wofi or fallback to terminal with `fzf`
launch_menu() {
	local _cmd=$1; shift

	if type wofi > /dev/null 2>&1; then
		wofi --show $_cmd &
		return
	fi

	case $_cmd in
		drun)
			echo "drun"
			desktop_list | fzf_menu
			;;
		run)
			echo "drun"
			executable_list | fzf_menu
			;;
		dmenu)
			echo "drun"
			fzf_menu
			;;
		*)
			exit 1
			;;
	esac
}

##########
# Print a friendly help message
help_msg() {
	echo "Usage: application.sh [OPTION] COMMAND"
	echo ""
	echo "Options:"
	echo ""
	echo "  -h|--help"
	echo "    Print this help message."
	echo ""
	echo ""
	echo "Commands:"
	echo ""
	echo "  desktop_run [APP_NAME]"
	echo "    Run a .desktop app"
	echo ""
	echo "  list_executables [APP_NAME]"
	echo "    Print out all executables"
	echo ""
	echo "  drun"
	echo "    List all names from the systems .desktop files"
	echo ""
	echo "  run"
	echo "    List all executables on the system and run in a terminal"
	echo ""
	echo "  dmenu"
	echo "    List from stdin"
	echo ""
	echo ""
} >&2

#==============================================================================
# Get Input and execut functions
#==============================================================================

###########
# Filter input. should be list_desktop, list_executables, desktop_run or the
# help flag [-h|--help]

if [ -z $1 ]; then
	>&2 echo "no command given"
	help_msg
	exit 0
fi

opts=$@
for opt in $opts; do
	case $opt in
		-h|--help)
			help_msg
			exit 0
			;;
		desktop_run)
			shift
			desktop_run $@
			exit 0
			;;
		list_executables)
			executable_list
			exit 0
			;;
		drun)
			launch_menu drun
			exit 0
			;;
		run)
			launch_menu run
			exit 0
			;;
		dmenu)
			shift
			launch_menu dmenu
			exit 0
			;;
		*)
			>&2 echo "invalid options given"
			help_msg
			exit 1
	esac
done
